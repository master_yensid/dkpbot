# DKPBot

Python-based DKP Management and Raid Registration Bot for Discord.  This Bot was built for the purposeof fast and easy DKP management for Classic WoW Guild <Full Clear> on Heartseeker.


This system runs entirely through Discord using a single bot.  It stores a dictionary of guild members mapped to DKP values.  When a raid is run, officers can create a raid and open it up for registration, adding members or allowing members to add themselves to the raid.  DKP can then be awarded to that specific raid while it is being run.

The current state of the DKP 'database' can be exported to a .csv file at any point, and can be used as a point of reference for any discrepencies.  Also, a backup file is created everytime the bot turns on.


Commands:\
(O) Means the command can be run only be run by someone with the "Officer" Role\
(A) Means the command can be run by anyone

(O) $addmember [name] - adds a guild member with optional starting DKP amount\
(O) $add [name] [amount] - adds DKP a specific guild member\
(O) $addguild [amount] - adds an amount of DKP to the entire guild roster\
(O) $addraid [raid] [amount]- adds an amount of DKP to all members of the specified active raid\
(O) $sub [name] [amount] - deducts an amount of DKP from a specific guild member\
(O) $subguild [name] [amount] - deducts an amount of DPK from all guild members\
(O) $subraid [raid] [amount] - deducts an amount of DPK from all raidmember of the specified active raid\
(O) $set [player] [amount] - sets a player's DKP to a specific amount\
(A) $dkp [optional: name] - reports the DKP of the caller or specified player\
(A) $dkpguild - reports the DKP of entire guild roster\
(A) $dkpraid [raid] - reports the DKP of the entire specified raid\
(O) $startraid [raid] - registers the existence of a raid\
(O) $stopraid [raid] - deletes the specified registered raid\
(A) $showraids - reports all active raids\
(O) $openraid [raid] - opens a raid up for member registration\
(O) $closeraid [raid] - closes registration for a specific raid\
(O) $regraider [name] [raid] - registers a guild member for a specific raid\
(A) $register [raid] - registers you for a specific raid\
(O) $removeraider [name] [raid] - removes a raider from a specified raid\
(O) $export - exports the current state of DKP to .csv file\
(O) $addmembers [*names] - adds as many names to the guild roster as supplied\
(O) $saveraid [raid] - saves a local .csv of DKP earned during the specified raid and also exports a .csv with the same information to the channel the command was called in\
(A) $dkpearned [raid] - reports how much DKP was earned by each member during the specified raid\
(O) $removegmember [name] - removed the guild member frost the guild roster and subsequently removes their DKP as well\
(A) $guildlist - lists all names in the guild roster\
(A) $raidlist [raid] - lists all names registered for a specific raid\
(O) $addraider [name] [raid] [amount] - adds a specified amount of DKP to a specified player in a specified raid\
(O) $subraider [name] [raid] [amount] - subtracts a specified amount of DKP from a specified player in a specified raid\
(O) $regraiders [raid] [names] - registers a list of space separated raiders for a specified raid\
(O) $saveraidfile - saves the current state of raids in a .json file

PREREQUISITES:\
Python 3\
discord.py (python discord library)

SETUP:\
Just clone the code and it should work simply by running:\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;python DKPBot.py\
Before you run, you will need to create a secrets dir to store your client and guild IDs.\
You will also need to create a reports dir to store your current DKP status and backups.\
If you don't have a role called "Officer" in your Discord, you'll need to adjust the code to reflect whatever the name of your administrative role is.