$(document).ready(function() {

    promise = $.ajax({
        type:"GET",
        dataType:"text",
        url:"http://fullclear.info/prices.csv",
        cache:false
    });

    promise.done(function(data){
        var dataArr = data.split("\n");
        var raw = "";
        var open = false;

        $.each(dataArr,function(){
            var rowData = this.split(",");
            
            if (rowData.length == 1) {
                if (open) {
                    $('#prices-container').append(raw);
                    raw = "</table><h2>" + this + "</h2><table>";
                }
                else {
                    raw = "<h2>" + this + "</h2><table>";
                }
                open = true;
            }
            else if (rowData.length == 2) {
                $('#prices-container').append(raw);
                raw = "</table><table class='raid-container'><tr><th class='class-header'>" + rowData[0] + "</th></tr><tr><th>Item</th><th class='price'>Price</th></tr>";
            }
            else if (rowData.length == 3) {
                raw += "<tr class='item-row'><td>" + rowData[0] + "</td><td class='price'>" + rowData[1] + "</td></tr>";
            }

        });
        raw += "</table>";
        $('#prices-container').append(raw);

    });

});